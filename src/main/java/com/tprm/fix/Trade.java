package com.tprm.fix;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;


public class Trade implements Serializable{

    private static final long serialVersionUID = 1L;


    public Trade(String id) {
        this.id = id;
    }

    public Trade() {}

    private String accountId;

    private String bbgCustAccount;

    private long bbgCustTraderUUID;

    private String bbgCustUserLogon;

    private long bbgCustUserNo;

    private String bbgSalesPersonUUID;

    private String bbgSalesPersonUUID2;

    private String bbgSalesPersonUUID3;

    private String bbgTicker;

    private long bbgTraderUUID;

    private String blockTrade;

    private String bookingId;

    private String clearingChannel;

    private String clearingCode;

    private String clearingHouse;

    private String clearingMember;

    private String clearingModel;

    private String clientOrderID;

    private String code;

    private String cpMember;

    private String cpMemberId;

    private String cpMemberName;

    private String cpMemberOriginal;

    private String cpTrader;

    private String currencyStr;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate date;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dateMaturity;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dateMktCreation;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dateSettl;

    private double dealerPrice;

    private String desc;

    private String id;

    private String instrumentId;

    private double principal;

    private double qty;

    private double qtyNominal;

    private String statusStr;

    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    private LocalTime time;

    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    private LocalTime timeCreation;

    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    private LocalTime timeCreationMarket;

    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    private LocalTime timeMktCreation;

    private double timeMktCreationUTC;

    private String typeStr;

    private long verb;

    private String verbStr;

    private double yDiffTickStatic;

    private double yield;


    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getBbgCustAccount() {
        return bbgCustAccount;
    }

    public void setBbgCustAccount(String bbgCustAccount) {
        this.bbgCustAccount = bbgCustAccount;
    }

    public long getBbgCustTraderUUID() {
        return bbgCustTraderUUID;
    }

    public void setBbgCustTraderUUID(long bbgCustTraderUUID) {
        this.bbgCustTraderUUID = bbgCustTraderUUID;
    }

    public String getBbgCustUserLogon() {
        return bbgCustUserLogon;
    }

    public void setBbgCustUserLogon(String bbgCustUserLogon) {
        this.bbgCustUserLogon = bbgCustUserLogon;
    }

    public long getBBGCustUserNo() {
        return bbgCustUserNo;
    }

    public void setBBGCustUserNo(long bbgCustUserNo) {
        this.bbgCustUserNo = bbgCustUserNo;
    }

    public String getBbgSalesPersonUUID() {
        return bbgSalesPersonUUID;
    }

    public void setBbgSalesPersonUUID(String bbgSalesPersonUUID) {
        this.bbgSalesPersonUUID = bbgSalesPersonUUID;
    }

    public String getBbgSalesPersonUUID2() {
        return bbgSalesPersonUUID2;
    }

    public void setBbgSalesPersonUUID2(String bbgSalesPersonUUID2) {
        this.bbgSalesPersonUUID2 = bbgSalesPersonUUID2;
    }

    public String getBbgSalesPersonUUID3() {
        return bbgSalesPersonUUID3;
    }

    public void setBbgSalesPersonUUID3(String bbgSalesPersonUUID3) {
        this.bbgSalesPersonUUID3 = bbgSalesPersonUUID3;
    }

    public String getBbgTicker() {
        return bbgTicker;
    }

    public void setBbgTicker(String bbgTicker) {
        this.bbgTicker = bbgTicker;
    }

    public long getBbgTraderUUID() {
        return bbgTraderUUID;
    }

    public void setBbgTraderUUID(long bbgTraderUUID) {
        this.bbgTraderUUID = bbgTraderUUID;
    }

    public String getBlockTrade() {
        return blockTrade;
    }

    public void setBlockTrade(String blockTrade) {
        this.blockTrade = blockTrade;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getClearingChannel() {
        return clearingChannel;
    }

    public void setClearingChannel(String clearingChannel) {
        this.clearingChannel = clearingChannel;
    }

    public String getClearingCode() {
        return clearingCode;
    }

    public void setClearingCode(String clearingCode) {
        this.clearingCode = clearingCode;
    }

    public String getClearingHouse() {
        return clearingHouse;
    }

    public void setClearingHouse(String clearingHouse) {
        this.clearingHouse = clearingHouse;
    }

    public String getClearingMember() {
        return clearingMember;
    }

    public void setClearingMember(String clearingMember) {
        this.clearingMember = clearingMember;
    }

    public String getClearingModel() {
        return clearingModel;
    }

    public void setClearingModel(String clearingModel) {
        this.clearingModel = clearingModel;
    }

    public String getClientOrderID() {
        return clientOrderID;
    }

    public void setClientOrderID(String clientOrderID) {
        this.clientOrderID = clientOrderID;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCpMember() {
        return cpMember;
    }

    public void setCpMember(String cpMember) {
        this.cpMember = cpMember;
    }

    public String getCpMemberId() {
        return cpMemberId;
    }

    public void setCpMemberId(String cpMemberId) {
        this.cpMemberId = cpMemberId;
    }

    public String getCpMemberName() {
        return cpMemberName;
    }

    public void setCpMemberName(String cpMemberName) {
        this.cpMemberName = cpMemberName;
    }

    public String getCpMemberOriginal() {
        return cpMemberOriginal;
    }

    public void setCpMemberOriginal(String cpMemberOriginal) {
        this.cpMemberOriginal = cpMemberOriginal;
    }

    public String getCpTrader() {
        return cpTrader;
    }

    public void setCpTrader(String cpTrader) {
        this.cpTrader = cpTrader;
    }

    public String getCurrencyStr() {
        return currencyStr;
    }

    public void setCurrencyStr(String currencyStr) {
        this.currencyStr = currencyStr;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalDate getDateMaturity() {
        return dateMaturity;
    }

    public void setDateMaturity(LocalDate dateMaturity) {
        this.dateMaturity = dateMaturity;
    }

    public LocalDate getDateMktCreation() {
        return dateMktCreation;
    }

    public void setDateMktCreation(LocalDate dateMktCreation) {
        this.dateMktCreation = dateMktCreation;
    }

    public LocalDate getDateSettl() {
        return dateSettl;
    }

    public void setDateSettl(LocalDate dateSettl) {
        this.dateSettl = dateSettl;
    }

    public double getDealerPrice() {
        return dealerPrice;
    }

    public void setDealerPrice(double dealerPrice) {
        this.dealerPrice = dealerPrice;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInstrumentId() {
        return instrumentId;
    }

    public void setInstrumentId(String instrumentId) {
        this.instrumentId = instrumentId;
    }

    public double getPrincipal() {
        return principal;
    }

    public void setPrincipal(double principal) {
        this.principal = principal;
    }

    public double getQty() {
        return qty;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }

    public double getQtyNominal() {
        return qtyNominal;
    }

    public void setQtyNominal(double qtyNominal) {
        this.qtyNominal = qtyNominal;
    }

    public String getStatusStr() {
        return statusStr;
    }

    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public LocalTime getTimeCreation() {
        return timeCreation;
    }

    public void setTimeCreation(LocalTime timeCreation) {
        this.timeCreation = timeCreation;
    }

    public LocalTime getTimeCreationMarket() {
        return timeCreationMarket;
    }

    public void setTimeCreationMarket(LocalTime timeCreationMarket) {
        this.timeCreationMarket = timeCreationMarket;
    }

    public LocalTime getTimeMktCreation() {
        return timeMktCreation;
    }

    public void setTimeMktCreation(LocalTime timeMktCreation) {
        this.timeMktCreation = timeMktCreation;
    }

    public double getTimeMktCreationUTC() {
        return timeMktCreationUTC;
    }

    public void setTimeMktCreationUTC(double timeMktCreationUTC) {
        this.timeMktCreationUTC = timeMktCreationUTC;
    }

    public String getTypeStr() {
        return typeStr;
    }

    public void setTypeStr(String typeStr) {
        this.typeStr = typeStr;
    }

    public long getVerb() {
        return verb;
    }

    public void setVerb(long verb) {
        this.verb = verb;
    }

    public String getVerbStr() {
        return verbStr;
    }

    public void setVerbStr(String verbStr) {
        this.verbStr = verbStr;
    }

    public double getyDiffTickStatic() {
        return yDiffTickStatic;
    }

    public void setyDiffTickStatic(double yDiffTickStatic) {
        this.yDiffTickStatic = yDiffTickStatic;
    }

    public double getYield() {
        return yield;
    }

    public void setYield(double yield) {
        this.yield = yield;
    }

    @Override
    public String toString() {
        return "com.tprm.fix.Trade [id=" + id + ", accountId=" + accountId + ", bbgCustAccount=" + bbgCustAccount
                + ", bbgCustTraderUUID=" + bbgCustTraderUUID + ", bbgCustUserLogon=" + bbgCustUserLogon
                + ", bbgCustUserNo=" + bbgCustUserNo + ", bbgSalesPersonUUID=" + bbgSalesPersonUUID
                + ", bbgSalesPersonUUID2=" + bbgSalesPersonUUID2 + ", bbgSalesPersonUUID3=" + bbgSalesPersonUUID3
                + ", bbgTicker=" + bbgTicker + ", bbgTraderUUID=" + bbgTraderUUID + ", blockTrade=" + blockTrade
                + ", bookingId=" + bookingId + ", clearingChannel=" + clearingChannel + ", clearingCode=" + clearingCode
                + ", clearingHouse=" + clearingHouse + ", clearingMember=" + clearingMember + ", clearingModel="
                + clearingModel + ", clientOrderID=" + clientOrderID + ", code=" + code + ", cpMember=" + cpMember
                + ", cpMemberId=" + cpMemberId + ", cpMemberName=" + cpMemberName + ", cpMemberOriginal="
                + cpMemberOriginal + ", cpTrader=" + cpTrader + ", currencyStr=" + currencyStr + ", date=" + date
                + ", dateMaturity=" + dateMaturity + ", dateMktCreation=" + dateMktCreation + ", dateSettl=" + dateSettl
                + ", dealerPrice=" + dealerPrice + ", desc=" + desc + ", instrumentId=" + instrumentId + ", principal="
                + principal + ", qty=" + qty + ", qtyNominal=" + qtyNominal + ", statusStr=" + statusStr + ", time="
                + time + ", timeCreation=" + timeCreation + ", timeCreationMarket=" + timeCreationMarket
                + ", timeMktCreation=" + timeMktCreation + ", timeMktCreationUTC=" + timeMktCreationUTC + ", typeStr="
                + typeStr + ", verb=" + verb + ", verbStr=" + verbStr + ", yDiffTickStatic=" + yDiffTickStatic
                + ", yield=" + yield + "]";
    }

}

