package com.tprm.fix;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FIXAdapter{

    private static Logger logger = LoggerFactory.getLogger(FIXAdapter.class);

    private FixTrade trade;

    public FIXAdapter() {

    }

    public void setTrade(FixTrade trade) {
        this.trade = trade;
    }

    public FixTrade getData() {
        return trade;
    }


}


