package com.tprm.fix;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.Arrays;

@SpringBootApplication
public class Main1 {
    public static void main(String[] args){

        SpringApplication.run(Main1.class, args);

    }

    @Bean
    public CommandLineRunner commandLineRunner(final ApplicationContext ctx){
        return args ->{
            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            for(String beanName: beanNames ) {
                //LOGGER.info(beanName);
            }
        };
    }
}
