package com.tprm.fix;

import org.quickfixj.jmx.JmxExporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import quickfix.ConfigError;
import quickfix.DefaultMessageFactory;
import quickfix.FileStoreFactory;
import quickfix.MessageFactory;
import quickfix.MessageStoreFactory;
import quickfix.RuntimeError;
import quickfix.SessionSettings;
import quickfix.SocketAcceptor;
import quickfix.mina.acceptor.DynamicAcceptorSessionProvider.TemplateMapping;

import javax.annotation.PostConstruct;
import javax.management.ObjectName;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class MainClass {

    @Autowired
    private KafkaTemplate kafkaTemplate;

    @Value("${kafka.trade.topic}")
    private String tradeTopic;

    private SocketAcceptor acceptor;
    private final Map<InetSocketAddress, List<TemplateMapping>> dynamicSessionMappings = new HashMap<>();

    private JmxExporter jmxExporter;
    private ObjectName connectorObjectName;

    public MainClass(){
        System.out.println("Anuraggggggggggg");
    }


    @PostConstruct
    public void init(){
        try{
            String [] args = new String []{};
            System.out.println(args.length);
            ClassLoader classLoader = getClass().getClassLoader();
            InputStream inputStream = new FileInputStream(classLoader.getResource("config").getFile());
            //file.ge
            //InputStream inputStream = getSettingsInputStream(args);
            SessionSettings settings = new SessionSettings(inputStream);
            inputStream.close();

            Application application = new Application(settings);
            application.setKafkaTemplate(kafkaTemplate);
            application.setTradeTopic(tradeTopic);
            MessageStoreFactory messageStoreFactory = new FileStoreFactory(settings);
            MessageFactory messageFactory = new DefaultMessageFactory();

            acceptor = new SocketAcceptor(application, messageStoreFactory, settings,
                    messageFactory);

            jmxExporter = new JmxExporter();
            connectorObjectName = jmxExporter.register(acceptor);
            this.start();

            System.out.println("press <enter> to quit");
            System.in.read();

            this.stop();
        }catch(Exception e){

        }
    }

    private void start() throws RuntimeError, ConfigError {
        acceptor.start();
    }

    private void stop() {
        acceptor.stop();
    }


    private static InputStream getSettingsInputStream(String[] args) throws FileNotFoundException {
        InputStream inputStream = null;
        if (args.length == 0) {
            inputStream = MainClass.class.getResourceAsStream("config");
        } else if (args.length == 1) {
            inputStream = new FileInputStream(args[0]);
        }
        if (inputStream == null) {
            System.out.println("usage: " + MainClass.class.getName() + " [configFile].");
            System.exit(1);
        }
        return inputStream;
    }
}