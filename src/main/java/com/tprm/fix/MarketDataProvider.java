package com.tprm.fix;

public interface MarketDataProvider {

    double getBid(String symbol);

    double getAsk(String symbol);
}