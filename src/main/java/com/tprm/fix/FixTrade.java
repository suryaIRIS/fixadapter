package com.tprm.fix;

import quickfix.FieldNotFound;
import quickfix.fix42.ExecutionReport;

import java.time.LocalDate;
import java.time.LocalTime;

public class FixTrade extends Trade{

    quickfix.fix42.ExecutionReport exReport;



    public FixTrade(ExecutionReport exReport) {
        this.exReport = exReport;
    }

    @Override
    public String getAccountId() {
        try {
            return String.valueOf(exReport.getAccount());



        } catch (FieldNotFound fieldNotFound) {
            return null;
        }
    }

    @Override
    public void setAccountId(String accountId) {
        super.setAccountId(accountId);
    }

    @Override
    public String getBbgCustAccount() {
        return super.getBbgCustAccount();
    }

    @Override
    public void setBbgCustAccount(String bbgCustAccount) {
        super.setBbgCustAccount(bbgCustAccount);
    }

    @Override
    public long getBbgCustTraderUUID() {
        return super.getBbgCustTraderUUID();
    }

    @Override
    public void setBbgCustTraderUUID(long bbgCustTraderUUID) {
        super.setBbgCustTraderUUID(bbgCustTraderUUID);
    }

    @Override
    public String getBbgCustUserLogon() {
        return super.getBbgCustUserLogon();
    }

    @Override
    public void setBbgCustUserLogon(String bbgCustUserLogon) {
        super.setBbgCustUserLogon(bbgCustUserLogon);
    }

    @Override
    public long getBBGCustUserNo() {
        return super.getBBGCustUserNo();
    }

    @Override
    public void setBBGCustUserNo(long bbgCustUserNo) {
        super.setBBGCustUserNo(bbgCustUserNo);
    }

    @Override
    public String getBbgSalesPersonUUID() {
        return super.getBbgSalesPersonUUID();
    }

    @Override
    public void setBbgSalesPersonUUID(String bbgSalesPersonUUID) {
        super.setBbgSalesPersonUUID(bbgSalesPersonUUID);
    }

    @Override
    public String getBbgSalesPersonUUID2() {
        return super.getBbgSalesPersonUUID2();
    }

    @Override
    public void setBbgSalesPersonUUID2(String bbgSalesPersonUUID2) {
        super.setBbgSalesPersonUUID2(bbgSalesPersonUUID2);
    }

    @Override
    public String getBbgSalesPersonUUID3() {
        return super.getBbgSalesPersonUUID3();
    }

    @Override
    public void setBbgSalesPersonUUID3(String bbgSalesPersonUUID3) {
        super.setBbgSalesPersonUUID3(bbgSalesPersonUUID3);
    }

    @Override
    public String getBbgTicker() {
        return super.getBbgTicker();
    }

    @Override
    public void setBbgTicker(String bbgTicker) {
        super.setBbgTicker(bbgTicker);
    }

    @Override
    public long getBbgTraderUUID() {
        return super.getBbgTraderUUID();
    }

    @Override
    public void setBbgTraderUUID(long bbgTraderUUID) {
        super.setBbgTraderUUID(bbgTraderUUID);
    }

    @Override
    public String getBlockTrade() {
        return super.getBlockTrade();
    }

    @Override
    public void setBlockTrade(String blockTrade) {
        super.setBlockTrade(blockTrade);
    }

    @Override
    public String getBookingId() {
        return super.getBookingId();
    }

    @Override
    public void setBookingId(String bookingId) {
        super.setBookingId(bookingId);
    }

    @Override
    public String getClearingChannel() {
        return super.getClearingChannel();
    }

    @Override
    public void setClearingChannel(String clearingChannel) {
        super.setClearingChannel(clearingChannel);
    }

    @Override
    public String getClearingCode() {
        return super.getClearingCode();
    }

    @Override
    public void setClearingCode(String clearingCode) {
        super.setClearingCode(clearingCode);
    }

    @Override
    public String getClearingHouse() {
        return super.getClearingHouse();
    }

    @Override
    public void setClearingHouse(String clearingHouse) {
        super.setClearingHouse(clearingHouse);
    }

    @Override
    public String getClearingMember() {
        return super.getClearingMember();
    }

    @Override
    public void setClearingMember(String clearingMember) {
        super.setClearingMember(clearingMember);
    }

    @Override
    public String getClearingModel() {
        return super.getClearingModel();
    }

    @Override
    public void setClearingModel(String clearingModel) {
        super.setClearingModel(clearingModel);
    }

    @Override
    public String getClientOrderID() {
        return super.getClientOrderID();
    }

    @Override
    public void setClientOrderID(String clientOrderID) {
        super.setClientOrderID(clientOrderID);
    }

    @Override
    public String getCode() {
        return super.getCode();
    }

    @Override
    public void setCode(String code) {
        super.setCode(code);
    }

    @Override
    public String getCpMember() {
        return super.getCpMember();
    }

    @Override
    public void setCpMember(String cpMember) {
        super.setCpMember(cpMember);
    }

    @Override
    public String getCpMemberId() {
        return super.getCpMemberId();
    }

    @Override
    public void setCpMemberId(String cpMemberId) {
        super.setCpMemberId(cpMemberId);
    }

    @Override
    public String getCpMemberName() {
        return super.getCpMemberName();
    }

    @Override
    public void setCpMemberName(String cpMemberName) {
        super.setCpMemberName(cpMemberName);
    }

    @Override
    public String getCpMemberOriginal() {
        return super.getCpMemberOriginal();
    }

    @Override
    public void setCpMemberOriginal(String cpMemberOriginal) {
        super.setCpMemberOriginal(cpMemberOriginal);
    }

    @Override
    public String getCpTrader() {
        return super.getCpTrader();
    }

    @Override
    public void setCpTrader(String cpTrader) {
        super.setCpTrader(cpTrader);
    }

    @Override
    public String getCurrencyStr() {
        return super.getCurrencyStr();
    }

    @Override
    public void setCurrencyStr(String currencyStr) {
        super.setCurrencyStr(currencyStr);
    }

    @Override
    public LocalDate getDate() {
        return super.getDate();
    }

    @Override
    public void setDate(LocalDate date) {
        super.setDate(date);
    }

    @Override
    public LocalDate getDateMaturity() {
        return super.getDateMaturity();
    }

    @Override
    public void setDateMaturity(LocalDate dateMaturity) {
        super.setDateMaturity(dateMaturity);
    }

    @Override
    public LocalDate getDateMktCreation() {
        return super.getDateMktCreation();
    }

    @Override
    public void setDateMktCreation(LocalDate dateMktCreation) {
        super.setDateMktCreation(dateMktCreation);
    }

    @Override
    public LocalDate getDateSettl() {
        return super.getDateSettl();
    }

    @Override
    public void setDateSettl(LocalDate dateSettl) {
        super.setDateSettl(dateSettl);
    }

    @Override
    public double getDealerPrice() {
        return super.getDealerPrice();
    }

    @Override
    public void setDealerPrice(double dealerPrice) {
        super.setDealerPrice(dealerPrice);
    }

    @Override
    public String getDesc() {
        return super.getDesc();
    }

    @Override
    public void setDesc(String desc) {
        super.setDesc(desc);
    }

    @Override
    public String getId() {
        return super.getId();
    }

    @Override
    public void setId(String id) {
        super.setId(id);
    }

    @Override
    public String getInstrumentId() {
        return super.getInstrumentId();
    }

    @Override
    public void setInstrumentId(String instrumentId) {
        super.setInstrumentId(instrumentId);
    }

    @Override
    public double getPrincipal() {
        return super.getPrincipal();
    }

    @Override
    public void setPrincipal(double principal) {
        super.setPrincipal(principal);
    }

    @Override
    public double getQty() {
        return super.getQty();
    }

    @Override
    public void setQty(double qty) {
        super.setQty(qty);
    }

    @Override
    public double getQtyNominal() {
        return super.getQtyNominal();
    }

    @Override
    public void setQtyNominal(double qtyNominal) {
        super.setQtyNominal(qtyNominal);
    }

    @Override
    public String getStatusStr() {
        return super.getStatusStr();
    }

    @Override
    public void setStatusStr(String statusStr) {
        super.setStatusStr(statusStr);
    }

    @Override
    public LocalTime getTime() {
        return super.getTime();
    }

    @Override
    public void setTime(LocalTime time) {
        super.setTime(time);
    }

    @Override
    public LocalTime getTimeCreation() {
        return super.getTimeCreation();
    }

    @Override
    public void setTimeCreation(LocalTime timeCreation) {
        super.setTimeCreation(timeCreation);
    }

    @Override
    public LocalTime getTimeCreationMarket() {
        return super.getTimeCreationMarket();
    }

    @Override
    public void setTimeCreationMarket(LocalTime timeCreationMarket) {
        super.setTimeCreationMarket(timeCreationMarket);
    }

    @Override
    public LocalTime getTimeMktCreation() {
        return super.getTimeMktCreation();
    }

    @Override
    public void setTimeMktCreation(LocalTime timeMktCreation) {
        super.setTimeMktCreation(timeMktCreation);
    }

    @Override
    public double getTimeMktCreationUTC() {
        return super.getTimeMktCreationUTC();
    }

    @Override
    public void setTimeMktCreationUTC(double timeMktCreationUTC) {
        super.setTimeMktCreationUTC(timeMktCreationUTC);
    }

    @Override
    public String getTypeStr() {
        return super.getTypeStr();
    }

    @Override
    public void setTypeStr(String typeStr) {
        super.setTypeStr(typeStr);
    }

    @Override
    public long getVerb() {
        return super.getVerb();
    }

    @Override
    public void setVerb(long verb) {
        super.setVerb(verb);
    }

    @Override
    public String getVerbStr() {
        return super.getVerbStr();
    }

    @Override
    public void setVerbStr(String verbStr) {
        super.setVerbStr(verbStr);
    }

    @Override
    public double getyDiffTickStatic() {
        return super.getyDiffTickStatic();
    }

    @Override
    public void setyDiffTickStatic(double yDiffTickStatic) {
        super.setyDiffTickStatic(yDiffTickStatic);
    }

    @Override
    public double getYield() {
        return super.getYield();
    }

    @Override
    public void setYield(double yield) {
        super.setYield(yield);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
